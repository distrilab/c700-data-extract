# C700 Data Extract

Jupyter notebook to extract light spectrum data from the images exported from a Sekonic C-700 SPECTROMASTER Spectrometer

The C-700 does not supoort exporting light spectrum data to CSV or any other easily managable data format. It only allows exporting the charts as images.
So I wrote this quick and dirty hack to extract the data from the images. 

## F.A.Q
### How to open the notebook in Jupyter Notebook ?
Run this in a terminal :
  `$ jupyter notebook ./C700_Data_Extract.ipynb`

### How to install Jupyter Notebook ?
http://jupyter.org/install
